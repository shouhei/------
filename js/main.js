enchant();

window.onload = function() {
  var game = new Game(900, 600);
  game.y = 10;
  game.preload('./img/background.png', './img/masu.png', './img/ayachi.png', './img/koga.png', './img/nori.png', './img/okkun.png', './img/maru.png');
  var position = -1;
  var sub_position = 0;

  game.onload = function() {
    var scene = game.rootScene;

    var MapBase = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 2, 0, 0, 0, 3, 0, 1, 0, 3, 0, 0, 0, 1, 0],
      [0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 3, 0],
      [0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 2, 0],
      [0, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 3, 0],
      [0, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0],
      [0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 2, 0],
      [0, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0],
      [0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0],
      [0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 3, 0],
      [0, 0, 0, 0, 2, 0, 1, 0, 3, 0, 0, 0, 3, 0, 1, 0, 3, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    var map = new Map(50, 50);//マスのサイズ指定
    map.image = game.assets['masu.png'];//マスの画像読み込み
    map.loadData(MapBase);//マップのロード

    var background = new Sprite(900, 600);
    background.image = game.assets['./img/background.png'];

    var Dice = 0;
    var dice_label = new Label('Dice:' + Dice);
    dice_label.x = 10;
    dice_label.y = 150;
    dice_label.color = 'red';
    dice_label.font = "bold 24px 'Impact'";

    var name;
      var player = new Sprite(50, 50);
    player.x = 200;
    player.y = 0;

    var character_num = Math.floor(Math.random() * 5);

    switch (character_num) {
      case 0:
        name = "ayachi";
        player.image = game.assets['./img/ayachi.png'];
        break;
      case 1:
        name = "koga";
        player.image = game.assets['./img/koga.png'];
        break;
      case 2:
        name = "nori";
        player.image = game.assets['./img/nori.png'];
        break;
      case 3:
        name = "okkun";
        player.image = game.assets['./img/okkun.png'];
        break;
      case 4:
        name = "maru";
        player.image = game.assets['./img/maru.png'];
        break;

    }

    var player_label = new Label('Player:' + name);
    player_label.x = 10;
    player_label.y = 100;
    player_label.color = 'red';
    player_label.font = "bold 24px 'Impact'";

    var position_label = new Label('現在のマス:Start');
    position_label.x = 10;
    position_label.y = 200;
    position_label.color = 'red';
    position_label.font = "bold 24px 'Impact'";

    var credit = 0;
    var credit_label = new Label('単位:' + credit);
    credit_label.x = 10;
    credit_label.y = 250;
    credit_label.color = 'red';
    credit_label.font = "bold 24px 'Impact'";

    var year = 1;
    var year_label = new Label(year + '回生');
    year_label.x = 10;
    year_label.y = 300;
    year_label.color = 'red';
    year_label.font = "bold 24px 'Impact'";

    var komyusho = 0;
    //コミュ
      var riazyu = 0; //リア充
    var semi = 0;
    //ゼミ判定
      var job = false; //就職
    var master = false;
    //院

      scene.addChild(background);
    scene.addChild(map);
    scene.addChild(player_label);
    scene.addChild(dice_label);
    scene.addChild(position_label);
    scene.addChild(credit_label);
    scene.addChild(year_label);
    scene.addChild(player);

    var stop_flag = [0, 0, 0];

    function move(x, y) {
      player.x = x;
      player.y = y;
    }

    function set_position(a, b) {
      position = a;
      sub_position = b;
    }

    function add_y(num) {
      player.y += num * 50;
    }

    function subtraction_y(num) {
      player.y -= num * 50;
    }

    function main_action(c, k, r, s) {
      credit += c;
      komyusho += k;
      riazyu += r;
      alert(s);

    }

    var status_obj = [
      {"n": 0, "c": 4, "k": 2, "r": 0,
        "text": "親友ができました！\nなかなかに馬が合う。\n大学デビュー成功だお～(^q^)"
      },
      {"n": 1, "c": -1, "k": 0, "r": 0,
        "text": "お洒落さに惹かれフランス語を履修！\n噂通りの鬼単で落第しました＼(^o^)／"
      },
      {"n": 2, "c": 0, "k": 2, "r": 2,
        "text": "サークルの新歓に参加。\n自分の居場所を発見！これで君もリア充の仲間入り^^"
      },
      {"n": 3, "c": 0, "k": 0, "r": -1,
        "text": "バイト探しわず。\n自分の希望と現実には差があったようだ\nとりあえずニート"
      },
      {"n": 4, "c": 2, "k": 0, "r": 0,
        "text": "初めての成績発表\n友達との協力でフル単！"
      },
      {"n": 5, "c": 0, "k": 0, "r": 0,
        "text": "カンニングがばれた！！！\n単位をすべて没収された"
      },
      {"n": 6, "c": -1, "k": 1, "r": 2,
        "text": "相模原祭でバンド演奏！\n夢中になりすぎて勉学が疎かに…まぁ楽しかったよね"
      },
      {"n": 7, "c": 0, "k": 2, "r": 2,
        "text": "クリスマス\n綺麗と噂の点火祭を見に行く"
      },
      {"n": 8, "c": -2, "k": 0, "r": 0,
        "text": "バレンタインにつき、リア充爆破テロ発生\nテロに巻き込まれ入院"
      },
      {"n": 9, "c": 4, "k": 2, "r": 2,
        "text": "夢の留学\n充実した海外生活を送って外国語も学べて春休みさいこー！"
      },
      {"n": 10, "c": 0, "k": 0, "r": 0,
        "text": "第１学年終了\n成績はいかがでしょうか？\n次からは２年生です"
      },
      {"n": 11, "c": 0, "k": -2, "r": 0,
        "text": "ガイダンスを出席\n友達は誰も来ていない\nぼっちなう"
      },
      {"n": 12, "c": -2, "k": 0, "r": 0,
        "text": "初めての再履修\nこれ以上落とさない様に気をつけなければgkbr"
      },
      {"n": 13, "c": 2, "k": 2, "r": 2,
        "text": "後輩にドヤ顔でアドバイス\nちょっと崇められる"
      },
      {"n": 14, "c": 2, "k": 1, "r": 2,
        "text": "サークルの夏合宿\n過去最高の盛り上がりを見せる"
      },
      {"n": 15, "c": 0, "k": 0, "r": 0,
        "text": "成績発表\n留年が少しずつ見えていませんか？"
      },
      {"n": 16, "c": 0, "k": 4, "r": 2,
        "text": "飲酒運転がばれる\n単位が没収される"
      },
      {"n": 17, "c": 1, "k": 0, "r": 2,
        "text": "うちの学校が箱根駅伝で快進撃！\n新年早々に良い知らせを聞きテンションが上がる"
      },
      {"n": 18, "c": 2, "k": 0, "r": 2,
        "text": "第一希望のゼミに受かる！！！\n将来に希望が持てる！？"
      },
      {"n": 19, "c": 1, "k": 2, "r": -2,
        "text": "ゲイ疑惑な教授のゼミに配属決定！\n果たしてどうなる！？！？！？\n┌（┌ ＾o＾）┐ﾎﾓｫ"
      },
      {"n": 20, "c": 1, "k": -2, "r": 0,
        "text": "イマイチなゼミに合格\n活気が無く将来が不安(((°Д°;;)))"
      },
      {"n": 21, "c": 0, "k": 0, "r": 0,
        "text": "成績発表\nゼミも決まり次は３年生です"
      },
      {"n": 22, "c": 2, "k": -2, "r": 0,
        "text": "ゼミでぼっちになる\n精神的につらい"
      },
      {"n": 23, "c": 2, "k": -1, "r": 2,
        "text": "授業で友人発見\nぼっち回避（・∀・）"
      },
      {"n": 24, "c": 0, "k": 0, "r": 2,
        "text": "バイトと学校の二重生活に疲れ果てる\n出席不足で授業についていけない"
      },
      {"n": 25, "c": 2, "k": 2, "r": 2,
        "text": "インターンに合格！！！\n技術や知識がレベルアップ！"
      },
      {"n": 26, "c": 0, "k": 0, "r": -2,
        "text": "インターンに遅刻\n怒られるだけで無く、母校の評判も落ちる"
      },
      {"n": 27, "c": 2, "k": 2, "r": 2,
        "text": "ボランティアに参加！\n意識が高くなる"
      },
      {"n": 28, "c": 2, "k": 1, "r": 0,
        "text": "鬼軍曹の英語特訓に出席\n精神、肉体ともに疲労"
      },
      {"n": 29, "c": 2, "k": 2, "r": 2,
        "text": "ゼミ合宿スタート！\n真剣に話し合いゼミ仲間と研鑽しあう"
      },
      {"n": 30, "c": 2, "k": 2, "r": 0,
        "text": "就活スタート\nスタートダッシュに成功！"
      },
      {"n": 31, "c": -2, "k": 2, "r": 0,
        "text": "就活に夢中になる\n学校へ行かなくなり単位を落とす"
      },
      {"n": 32, "c": 0, "k": 0, "r": 0,
        "text": "成績発表！\n卒業できるの？"
      },
      {"n": 33, "c": -1, "k": -2, "r": 0,
        "text": "就活が筆記で落ちる\n学力の無さに落胆"
      },
      {"n": 34, "c": 0, "k": 0, "r": -2,
        "text": "就活が面接で落ちる\nコミュ力の無さに落胆"
      },
      {"n": 35, "c": 2, "k": 2, "r": 2,
        "text": "内定ゲット！!!\n後はきちんと卒業するだけ！"
      },
      {"n": 36, "c": 0, "k": 0, "r": -2,
        "text": "健康診断を受診\n肝臓の異常で内定取り消し(^q^)まじか"
      },
      {"n": 37, "c": 2, "k": 2, "r": 2,
        "text": "就活に成功！\n紆余曲折を経て社会人に！？"
      },
      {"n": 38, "c": 2, "k": 0, "r": 2,
        "text": "再履修をすべて回収！\n卒業はほぼ確定!?学生生活も後少し！"
      },
      {"n": 39, "c": 0, "k": 0, "r": 0,
        "text": "必修の英語が落第\n大丈夫なのか！？"
      },
      {"n": 40, "c": 2, "k": 0, "r": 0,
        "text": "院試に合格！\nまだまだ学生か！？"
      },
      {"n": 41, "c": 0, "k": -1, "r": 0,
        "text": "内定先がブラックと発覚＼(^o^)／\n行き先が思いやられる"
      },
      {"n": 42, "c": 0, "k": 0, "r": 0,
        "text": "テスト"
      },
      {"n": 43, "c": 0, "k": 0, "r": 0,
        "text": "テスト"
      }
    ];

    function graduation() {
      alert("卒業の判定をします！");
      if (credit >= 13) {
        var status = year_check();
        jm_check(status);
        riazyu_check();
      } else {
        position = 32;
        move(700, 500);
        year += 1;
        alert("単位が足りていないので４年生をもう一度挑戦！");
      }
    }


    function year_check() {
      if (year < 6) {
        alert("卒業おめでとうございます！");
        return true;
      }
      else if (year < 7) {
        alert("卒業まで時間かかり過ぎじゃないですか？");
        return true;
      }
      else if (year <= 8) {
        alert("卒業する気があったんですか？");
        return true;
      } else {
        alert("卒業までの期限を越えてますので、卒業とは認められません。");
        return false;
      }

    }

    function jm_check(status) {
      alert("進路の判定をします！");
      if (status) {
        if ((job === true) && (master === false)) {
          alert("就職おめでとうございます！");
        } else if ((job === false) && (master === true)) {
          alert("院に進学ですね！");
        } else if ((job === false) && (master === false)) {
          alert("就職せず院にも行かず、どうするつもりですか？");
        }
      } else {
        alert("卒業してないのに「大学生」として\n進路を見出そうとするのは\nずるいんじゃないかな？");
      }
    }

    function riazyu_check() {
      if (riazyu < 10) {
        alert("大学時代は楽しめていましたか？");
      } else if (riazyu >= 10) {
        alert("大学でも楽しめてましたか？");
      }
    }

    function komyusho_check() {
      if (komyusho < 10) {
        alert("友達はいましたか？");
      } else if (komyusho >= 10) {
        alert("親友は得られましたか？");
      }
    }


    game.rootScene.ontouchstart = function() {
      if (position >= 42) {
        position_label.text = '現在のマス:Goal';
        position = 42;
        graduation();
      }else{
      dice_label.text = 'Dice:' + 0;
      Dice = Math.floor(Math.random() * 3 + 1);
      dice_label.text = 'Dice:' + Dice;
      alert(Dice + "マス進みます\nその最中に・・・");
      position += Dice;
      if(position >= 42){
        position = 42;
      }

      if (position <= 9) {
        add_y(Dice);
      }
      else if (position > 9 && sub_position === 0) {
        set_position(10, 1);
        move(300, 500);
      }
      else if (position > 10 && position <= 20) {
        if (stop_flag[0] < 1) {
          move(player.x + 100, 550);
          stop_flag[0] = 1;
        }
        subtraction_y(Dice);
      }
      else if (position > 20 && sub_position === 1) {
        set_position(21, 2);
        move(500, 50);
      }
      else if (position > 21 && position <= 31) {
        move(600, player.y);
        if (stop_flag[1] < 1) {
          move(player.x, 0);
          stop_flag[1] = 1;
        }
        add_y(Dice);
      }
      else if (position > 31 && sub_position === 2) {
        set_position(32, 3);
        move(700, 500);
      }
      else if (position > 32 && position <= 41) {
        move(800, player.y);
        if (stop_flag[2] < 1) {
          move(player.x, 550);
          stop_flag[2] = 1;
        }
        subtraction_y(Dice);
      }
      else {
        move(800, 50);
      }

      dice_label.text = 'Dice:' + 0;
      position_label.text = '現在のマス:' + (position + 1);

      var c = status_obj[position]["c"];
      var k = status_obj[position]["k"];
      var r = status_obj[position]["r"];
      var s = status_obj[position]["text"];
      if (position < 42) {
        main_action(c, k, r, s);
        if (position === 5) {
          credit = 0;
          position = -1;
          move(200, 0);
          year += 1;
          year_label.text = year + '回生';
          alert("テストは自分の力でがんばりましょう。\n1年生をやり直し");
        } else if (position === 16) {
          stop_flag[0] = 0;
          year += 1;
          year_label.text = year + '回生';
          position = 10;
          move(300, 500);
          main_action(0, 4, 2, "飲酒運転は5年以下の懲役又は100万円以下の罰金です\n2年生をもう一度やり直してください");
          credit = 0;
        } else if (position === 39) {
          stop_flag[2] = 0;
          position = 32;
          move(700, 500);
          year += 1;
          year_label.text = year + '回生';
          alert("大丈夫ではありません。\n4年生をもう一度");
        } else if (position === 10 || position === 21 || position === 32) {
          year += 1;
          year_label.text = year + '回生';
        } else if (position === 35) {
          job = true;
        } else if (position === 33 || position === 34 || position === 36 || position === 37) {
          job = false;
        } else if (position === 40) {
          master = true;
        }
        position_label.text = '現在のマス:' + (position + 1);
        credit_label.text = '単位:' + credit;
      } else {
        position = 42;
        position_label.text = '現在のマス:Goal';
        credit_label.text = '単位:' + credit;
        graduation();
      }


    };
    };

  };
  game.start();
};



